# Teams and Projects

**Team 1:**  
Description:  
Members:

- Scott Low [[scouter32@gmail.com](mailto:scouter32@gmail.com)]
- Samuel Navarrete [[cakebrewery@gmail.com](mailto:cakebrewery@gmail.com)]
- Laura Grondahl [[emailgrondahl@gmail.com](mailto:emailgrondahl@gmail.com)]
- Riz Panjwani [[panjwani.riz@gmail.com](mailto:panjwani.riz@gmail.com)]
- Carly Lebeuf [[carly.lebeuf@gmail.com](mailto:carly.lebeuf@gmail.com)]

---

**Team 2:**  
Description:  
Members:

- Student 1 <email>
- Student 2 <email>
- Student 3 <email>
- Student 4 <email>

---

**Team 3:**  
Description:  
Members:

- Student 1 <email>
- Student 2 <email>
- Student 3 <email>
- Student 4 <email>

---

**Team 4:**  
Description:  
Members:

- Student 1 <email>
- Student 2 <email>
- Student 3 <email>
- Student 4 <email>

---

**Team 5:**  
Description:  
Members:

- Student 1 <email>
- Student 2 <email>
- Student 3 <email>
- Student 4 <email>

---

## Past Teams and Projects

## Tel Aviv University Course

- [2007/2008, Semester A](http://tau-itw.wikidot.com/active-projects-08)
- [2007/2008, Semester B](http://tau-gadgets.wikidot.com/)
- [2008/2009, Semester A](http://sites.google.com/site/taugadgets09a/)
- [2008/2009, Semester B](http://sites.google.com/site/taugadgets09b/)
- [2009/2010, Semester A](http://sites.google.com/site/taugadgets10a)
- [2009/2010, Semester B](http://sites.google.com/site/taugadgets10b)
- [2010/2011, Semester A](https://sites.google.com/site/cloudweb10a/)
- [2010/2011, Semester B](https://sites.google.com/site/cloudweb10b)
- [2011/2012, Semester A](https://sites.google.com/site/cloudweb11a/)
- [2011/2012, Semester B](https://sites.google.com/site/cloudweb11b/)
- [2012/2013, Semester A](https://sites.google.com/site/cloudweb12a/)
- [2012/2013, Semester B](https://sites.google.com/site/cloudweb12b/)